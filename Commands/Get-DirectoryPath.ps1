function Get-DirectoryPath(){
  $result = @()
  $args | ? { Test-Path $_ } | % {
    $result += (ls $_).Directory
  }
  $result
}

function Get-DirectoryPathString(){
  (getdir $args)[0].FullName
}

Set-Alias getdir Get-DirectoryPath
Set-Alias getdirstr Get-DirectoryPathString
