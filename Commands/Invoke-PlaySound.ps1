function Invoke-PlaySound(){
  if(($args.Count -eq 1) -and (Test-Path -path $args[0]) -and ((ls $args[0]).Extension.ToLower() -eq ".wav")){
    (New-Object Media.SoundPlayer((ls $args[0]).FullName)).Play()
  }
}

function Invoke-PlayBell(){ Invoke-PlaySound ((getdirstr $Profile) + "\Resource\Bell.wav") }

function Invoke-PlayOwari(){ Invoke-PlaySound ((getdirstr $Profile) + "\Resource\Owari.wav") }
