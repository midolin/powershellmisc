function Get-CommandLocation(){
  $result = @()
  foreach($item in $args){
    $obj = [pscustomobject]@{Command="$item";Result="";Type=""}
    try{
      $gc = get-command $item -ErrorAction stop
      $obj.Type = $gc.CommandType
      if($gc.CommandType -eq "Application"){
        $obj.Result = $gc.Definition
      }else{
        $obj.Result = $gc.DisplayName
      }
    } catch{
      $obj.Result = "Not Found."
    }
    $result += $obj
  }
  $result
}
