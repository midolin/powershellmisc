function Show-Pathes(){
  foreach($item in $env:path.Split(";")){
    if($item.Contains("%")){
      $envname = $item.split("%")[1].Trim()
      $resolve = (get-item env:$envname).value
      write-host (joinarrow $item $resolve) -nonewline
    }else{
      write-host -nonewline $item
    }
    write-host ""
  }
}

function Get-Pathes(){
  $result = @()
  foreach($item in $env:path.Split(";")){
    if($item.Contains("%")){
      $envname = $item.split("%")[1].Trim()
      $resolve += (get-item env:$envname).value
    } else {
      $result += $item
    }
  }
  $result
}
