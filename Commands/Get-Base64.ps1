function Get-Base64(){
  $result = @();
  foreach($item in $args){
    if(Test-Path $item){
      Write-Host "Get Base64 from file '$item' ..."
      $result += [Convert]::ToBase64String([System.IO.File]::ReadAllBytes((ls $item).FullName))
    } else {
      Write-Host "Get Base64 from string(UTF8) '$item' ..."
      $result += [Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($item))
    }
  }
  $result;
}
