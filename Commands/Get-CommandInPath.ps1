# Find specified value in path
function Get-CommandInPath(){
  $result = @()
  foreach($item in $args){
    foreach ($dir in Get-Pathes){
      if(Test-Path "$dir\$item.*"){
        ls "$dir\$item.*" | ? {
           $env:pathext.Split(";") -contains $_.Extension.ToUpper()
        } | % { $result += $_ }
      }
    }
  }
  $result
}

function Show-CommandInPath(){
  $args | % {
    $result = (Get-CommandInPath $_)
    if($result.Count -eq 1){
      (joinarrow $_ $result.FullName)
    }else {
      if($result.Count -gt 1){
        "$_ :"
        $result | % { "    $_" }
      }else{
      "'$_' is not found."
      }
    }
  }
}
