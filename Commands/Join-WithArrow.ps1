function Join-WithArrow(){
  $result = ""
  for($index = 0; $index -lt $args.Count; $index++){
    $result += $args[$index]
    if($index -ne ($args.Count - 1)){
      $result += " -> "
    }
  }
  $result
}
