pushd ((ls $Profile).Directory.FullName + "\Modules\Misc")

# 関数から読み込む(functionを定義してその中でscriptをincludeする)とダメっぽいので、
# 直接読む

# Load scripts in module dir.
ls Commands\*.ps1 | % { . (ls $_).FullName }

Set-Alias printpath Show-Pathes
Set-Alias which Get-CommandLocation
Set-Alias play Invoke-PlaySound
Set-Alias finish Invoke-PlayBell
Set-Alias owari Invoke-PlayOwari
Set-Alias joinarrow Join-WithArrow
set-Alias addpath Add-Path


# expose functionsS
Export-ModuleMember -Function *
Export-ModuleMember -Alias *

popd
