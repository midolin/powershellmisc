PowerShell Misc scripts
====

Windows PowerShellを少しだけ使いやすくするためのスクリプト集です。

## 概要
whichコマンドなど、PowerShellを使っていて少し物足りないと感じていたコマンドを
追加するためのリポジトリです。

何がどう物足りないかは作者の感覚に依存しています。

## 使い方
1. リポジトリをModulesフォルダに複製します。
2. `$Profile`ファイルに`Import-Module Misc`を追加します。
3. PowerShellを実行します。

### 拡張方法
`Misc\Commands`フォルダにコマンドが記述されたスクリプトファイルが置かれています。
ここに`.ps1`ファイルを配置すると、`Misc.psm1`が関数とエイリアスを読み込み、
自動で使えるようにします。

## ライセンス
MIT.

## 作者
[limemidolin](https://twitter.com/limemidolin)
